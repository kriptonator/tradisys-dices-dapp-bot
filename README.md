# Tradisys Dices dAapp bot

This is simple UI-bot, which allows to play and follow for the yours leaderboard movement!

## Customization
First go to the `src/config.js` and set your acoount `seed` and `address`
```
  dapp: {
    address: '3MwNdzhruMKbbt7sECBBWu223a8qpbEnswt',
  },
  player: {
    seed: '{ACCOUNT_SEED}',
    address: '{ACCOUNT_ADDRESS}',
  },
```
Then you can begin project setup. After launch, go to the `http://localhost:8080/ ` (port may be different), and press `start` button.
Good luck!

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
