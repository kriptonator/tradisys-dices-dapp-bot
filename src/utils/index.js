import { base58encode } from '@waves/waves-crypto';
import uuidv4 from 'uuid/v4';
import * as States from './states';

export const uuid = () => uuidv4();

export const toBase58 = input => base58encode(input);

export const jsonHeaders = {
  Accept: 'application/json',
  'Content-Type': 'application/json',
};

export const wait = ms => new Promise(r => setTimeout(r, ms));

export const getRandomDices = (dicesAmount) => {
  const getRandomFromRange = (min, max) => Math.floor(min + Math.random() * (max + 1 - min));
  const dices = [];
  while (dices.length < dicesAmount) {
    const value = getRandomFromRange(1, 6);
    if (!dices.includes(value)) {
      dices.push(value);
    }
  }
  return dices.sort();
};

export const dicesToString = dicesArray => dicesArray.join('');

export const getGameId = () => toBase58(uuid());

export { States };
