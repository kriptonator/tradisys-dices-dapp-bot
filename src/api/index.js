import urlJoin from 'url-join';
import config from '@/config';
import { jsonHeaders } from '@/utils';

const nodeUrl = 'https://testnodes.wavesnodes.com';
const backUrl = 'https://gamesapi.tradisys.com/api';

export const broadcastTx = async (tx) => {
  const response = await fetch(urlJoin(
    nodeUrl,
    '/transactions',
    '/broadcast',
  ), {
    method: 'POST',
    headers: jsonHeaders,
    body: tx,
  });

  return response.json();
};

export const getGameResultById = async (
  id,
) => {
  const response = await fetch(urlJoin(
    nodeUrl,
    '/addresses',
    '/data',
    config.dapp.address,
    id,
  ), {
    method: 'GET',
    headers: jsonHeaders,
  });

  return response.json();
};

export const getAddressBalanceInfo = async (
  address,
) => {
  const response = await fetch(urlJoin(
    nodeUrl,
    '/addresses',
    '/balance',
    '/details',
    address,
  ), {
    method: 'GET',
    headers: jsonHeaders,
  });

  return response.json();
};

export const getStatsLastGames = async () => {
  const response = await fetch(urlJoin(
    backUrl,
    '/stats',
    '/DICES_DAPP',
    '/leaders',
  ), {
    method: 'GET',
    headers: jsonHeaders,
  });

  return response.json();
};
