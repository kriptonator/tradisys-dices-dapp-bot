import {
  wait, dicesToString, getGameId, getRandomDices, States,
} from '@/utils';
import { invokeScript } from '@waves/waves-transactions';
import * as Mutations from './mutations-types';
import * as Actions from './actions-types';
import * as Api from '@/api';
import config from '@/config';

export default {
  async setGamesCount({ commit }, payload) {
    commit(Mutations.SET_GAMES_COUNT, payload);
  },
  async setBetAmount({ commit }, payload) {
    commit(Mutations.SET_BET_AMOUNT, payload);
  },
  async setDicesCount({ commit }, payload) {
    commit(Mutations.SET_DICES_COUNT, payload);
  },
  async pause({ commit }, payload) {
    commit(Mutations.SET_PAUSED, payload);
  },
  async startLifecycle({ dispatch }) {
    dispatch(Actions.LIFECYCLE);
  },
  async [Actions.BET]({ commit, dispatch, getters }) {
    const { dicesCount, betAmount } = getters;
    const gameId = getGameId();
    const dices = getRandomDices(dicesCount);
    const amount = betAmount * config.wave;
    const payment = {
      amount: amount + 500000,
      assetId: null,
    };
    const func = 'bet';
    const args = [
      {
        type: 'string',
        value: gameId,
      },
      {
        type: 'string',
        value: dicesToString(dices),
      },
    ];

    const invocation = invokeScript({
      dApp: config.dapp.address,
      chainId: config.api.network,
      call: {
        function: func,
        args,
      },
      payment: [payment],
      fee: 5000000,
    }, config.player.seed);

    try {
      await Api.broadcastTx(JSON.stringify(invocation));
      const payload = {
        id: gameId,
        amount: payment.amount,
        dices,
        state: States.SUBMITTED,
      };

      commit(Mutations.ADD_SUBMITTED_GAME, payload);
      dispatch(Actions.GAME_STATE_RESOLVE, payload);
    } catch (e) {
      console.log(e);
    }
  },
  async [Actions.LIFECYCLE]({ commit, getters, dispatch }) {
    try {
      const {
        submittedGames,
        paused,
        gamesCount,
        betAmount,
      } = getters;

      const { available } = await Api.getAddressBalanceInfo(config.player.address);
      commit(Mutations.SET_BALANCE, available);

      const leaders = await Api.getStatsLastGames();
      commit(Mutations.SET_LEADERS, leaders);

      if (!paused) {
        const max = gamesCount;
        if (available >= betAmount * config.wave + 500000) {
          if (submittedGames.length < max) {
            for (let i = 0; i < max - submittedGames.length; i += 1) {
              dispatch(Actions.BET);
            }
          }
        }
      }
    } catch (e) {
      // ignore errors
    } finally {
      await wait(3000);
      dispatch(Actions.LIFECYCLE);
    }
  },
  async [Actions.GAME_STATE_RESOLVE]({ commit }, payload) {
    let gameWinner = null;
    do {
      try {
        // Wait one second before poll:
        // eslint-disable-next-line
        await wait(1000);
        // Get game info for current state of the match.
        // Exit from cycle if winner is defined.
        // eslint-disable-next-line
        const resp = await Api.getGameResultById(payload.id);
        if (!resp.error && (resp.value === States.WON || resp.value === States.LOST)) {
          gameWinner = resp.value;
        }
      } catch (e) {
        // ignore error
      }
    } while (!gameWinner);
    commit(Mutations.REM_SUBMITTED_GAME, payload);
    commit(Mutations.ADD_PLAYED_GAME, {
      ...payload,
      state: gameWinner,
    });
  },
};
